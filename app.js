var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var nodemailer = require('nodemailer');

var app = express();

var Datastore = require('nedb');
global.db = new Datastore({ filename: './approvals-db', autoload: true });

global.approvalGroups = require('./approval-groups.json');

var newApproval = require('./routes/new-approval');
var approve = require('./routes/approve');

var mailTransporter = nodemailer.createTransport({
    host: process.env.SMTP_HOST,
    port: process.env.SMTP_PORT,
    secure: process.env.SMTP_SECURE, // upgrade later with STARTTLS
    auth: {
        user: process.env.SMTP_USER,
        pass: process.env.SMTP_PASSWORD
    }
});

mailTransporter.verify(function(error, success) {
    if (error) {
        console.log("NODEMAILER: Did not come up. E-Mails wont be sent.");
        console.error(error);
    } else {
        console.log("NODEMAILER: Server is ready to take our messages.");
    }
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/new-approval', newApproval);
app.use('/approve', approve);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;