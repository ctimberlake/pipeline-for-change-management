var express = require('express');
var router = express.Router();
var hash = require('hash-anything').sha1;

router.post('/', function(req, res, next) {
    if (!req.body.pipeline_url || !req.body.pipeline_id || !req.body.group) {
        res.status(500).send({ error: 'You\'re missing one of more of the following => `pipeline_url`, `pipeline_id`, `group`' });
        return;
    }

    var approvalID = hash(req.body.pipeline_url + req.body.pipeline_id + req.body.group);



    var _tmp_approvers = [];
    /* Generate Unique Hashes */
    for (item in approvalGroups[req.body.group]) {
        approvalGroups[req.body.group][item].hash = hash(new Date() + approvalGroups[req.body.group][item].email);
        approvalGroups[req.body.group][item].approved = false;
        approvalGroups[req.body.group][item].timeapproved = "";
        _tmp_approvers.push(approvalGroups[req.body.group][item]);
    };

    console.log(_tmp_approvers);

    var doc = {
        approvalID: approvalID,
        pipelineUrl: req.body.pipeline_url,
        pipelineId: req.body.pipeline_id,
        group: req.body.group,
        today: new Date(),
        approvers: _tmp_approvers
    };

    db.insert(doc, function(err, newDoc) {
        if (err) {
            console.error("An error saving a document occured:")
            console.error(err);
        }
        console.log("Pipeline: " + req.body.pipeline_id + " Approval Process Created");
        res.send({
            "status": "created",
            "url": process.env.BOT_URL + "/approve/" + newDoc._id
        });
        return;
    });

    //res.render('index', { title: 'Express' });
});

module.exports = router;