var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/:jobid/:approvalid', function(req, res, next) {


    db.findOne({ _id: req.params.jobid }, function(err, doc) {
        if (err) {
            console.error(err);
            return;
        }
        res.render('approval_page', { approvals: doc.approvers });
    });
});

router.post('/:jobid/:approvalid', function(req, res, next) {
    db.findOne({ _id: req.params.jobid }, function(err, doc) {
        var oldDoc = doc;

        doc.approvers.forEach(function(item, index) {
            console.log(req.body);
            if (item.hash == req.params.approvalid) {

                if (req.body.decision == "Approve Changes") {
                    item.timeapproved = new Date();
                    item.approved = true;
                } else if (req.body.decision == "Decline Approval") {
                    item.approved = false;
                    item.timeapproved = new Date();
                }
                doc.approvers[index] = item;
                db.update({ _id: req.params.jobid }, { $set: { approvers: doc.approvers } }, { multi: true },
                    function(err, numReplaced) {
                        //if (err) {
                        console.log(err);
                        //}
                        console.log(numReplaced);
                    });
            }
        });
        if (err) {
            console.error(err);
            return;
        }
        res.render('approval_page', { approvals: doc.approvers });
    });
});

module.exports = router;